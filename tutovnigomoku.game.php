<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * tutovnigomoku implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * tutovnigomoku.game.php
 *
 * This is the main file for your game logic.
 *
 * In this PHP file, you are going to defines the rules of the game.
 *
 */


require_once(APP_GAMEMODULE_PATH . 'module/table/table.game.php');


class tutovnigomoku extends Table
{
    function __construct()
    {
        // Your global variables labels:
        //  Here, you can assign labels to global variables you are using for this game.
        //  You can use any number of global variables with IDs between 10 and 99.
        //  If your game has options (variants), you also have to associate here a label to
        //  the corresponding ID in gameoptions.inc.php.
        // Note: afterwards, you can get/set the global variables with getGameStateValue/setGameStateInitialValue/setGameStateValue
        parent::__construct();

        self::initGameStateLabels([
            'end_of_game' => 10,
        ]);
    }

    protected function getGameName()
    {
        // Used for translations and stuff. Please do not modify.
        return "tutovnigomoku";
    }

    /*
        setupNewGame:
        
        This method is called only once, when a new game is launched.
        In this method, you must setup the game according to the game rules, so that
        the game is ready to be played.
    */
    protected function setupNewGame($players, $options = [])
    {
        // Set the colors of the players with HTML color code
        // The default below is red/green/blue/orange/brown
        // The number of colors defined here must correspond to the maximum number of players allowed for the gams
        $gameinfos = self::getGameinfos();
        $default_colors = ['000000', 'ffffff'];

        // Create players
        // Note: if you added some extra field on "player" table in the database (dbmodel.sql), you can initialize it there.
        $sql = "INSERT INTO player (player_id, player_color, player_canal, player_name, player_avatar) VALUES ";
        $values = [];
        foreach ($players as $player_id => $player) {
            $color = array_shift($default_colors);
            $values[] = "('" . $player_id . "','$color','" . $player['player_canal'] . "','" . addslashes(
                    $player['player_name']
                ) . "','" . addslashes($player['player_avatar']) . "')";
        }
        $sql .= implode(',', $values);
        self::DbQuery($sql);
        self::reloadPlayersBasicInfos();

        /************ Start the game initialization *****/

        // Init global values with their initial values
        //self::setGameStateInitialValue( 'my_first_global_variable', 0 );

        // Init game statistics
        // (note: statistics used in this file must be defined in your stats.inc.php file)
        //self::initStat( 'table', 'table_teststat1', 0 );    // Init a table statistics
        //self::initStat( 'player', 'player_teststat1', 0 );  // Init a player statistics (for all players)

        // TODO: setup the initial game situation here
        // Insert (empty) intersections into database
        $sql = "INSERT INTO intersection (coord_x, coord_y) VALUES ";
        $values = [];
        for ($x = 0; $x < 19; $x++) {
            for ($y = 0; $y < 19; $y++) {
                $values[] = "($x, $y)";
            }
        }
        $sql .= implode(',', $values);
        self::DbQuery($sql);

        self::setGameStateInitialValue('end_of_game', 0);

        // Black plays first
        $sql = "SELECT player_id, player_name FROM player WHERE player_color = '000000' ";
        $black_player = self::getNonEmptyObjectFromDb($sql);

        self::activeNextPlayer();

        $this->gamestate->changeActivePlayer($black_player['player_id']);
        /************ End of the game initialization *****/
    }

    /*
        getAllDatas: 
        
        Gather all informations about current game situation (visible by the current player).
        
        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)
    */
    protected function getAllDatas()
    {
        $result = [];

        $current_player_id = self::getCurrentPlayerId(
        );    // !! We must only return information visible by this player !!

        // Get information about players
        // Note: you can retrieve some extra field you added for "player" table in "dbmodel.sql" if you need it.
        $sql = "SELECT player_id id, player_score score FROM player ";
        $result['players'] = self::getCollectionFromDb($sql);

        // TODO: Gather all information about current game situation (visible by player $current_player_id).
        // Intersections
        $sql = "SELECT id, coord_x, coord_y, stone_color FROM intersection ";
        $result['intersections'] = self::getCollectionFromDb($sql);

        // Constants
        $result['constants'] = $this->gameConstants;

        // Counters
        $result['counters'] = $this->getGameCounters($current_player_id);

        return $result;
    }

    /*
        getGameProgression:
        
        Compute and return the current game progression.
        The number returned must be an integer between 0 (=the game just started) and
        100 (= the game is finished or almost finished).
    
        This method is called each time we are in a game state with the "updateGameProgression" property set to true 
        (see states.inc.php)
    */
    function getGameProgression()
    {
        // Compute and return the game progression

        // Number of stones laid down on the goban over the total number of intersections * 100
        $sql = "
	    	SELECT round(100 * count(id) / (19 * 19) ) as value from intersection WHERE stone_color is not null
    	";
        $counter = self::getNonEmptyObjectFromDB($sql);

        return $counter['value'];
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Utility functions
////////////    

    /*
        In this space, you can put any utility methods useful for your game logic
    */

    /*
        getGameCounters:

        Gather all relevant counters about current game situation (visible by the current player).
    */
    function getGameCounters($player_id)
    {
        $sql = "
    		SELECT
    			concat('stonecount_p', cast(p.player_id as char)) counter_name,
    			case when p.player_color = 'white' then 180 - count(id) else 181 - count(id) end counter_value
    		FROM (select player_id, case when player_color = 'ffffff' then 'white' else 'black' end player_color FROM player) p
    		LEFT JOIN intersection i on i.stone_color = p.player_color
    		GROUP BY p.player_color, p.player_id
    	";
        if ($player_id != null) {
            // Player private counters: concatenate extra SQL request with UNION using the $player_id parameter
        }

        return self::getNonEmptyCollectionFromDB($sql);
    }

    public function checkForWin(int $coord_x, int $coord_y, string $color): bool
    {
        return random_int(0, 10) < 3;
    }


//////////////////////////////////////////////////////////////////////////////
//////////// Player actions
//////////// 

    /*
        Each time a player is doing some game action, one of the methods below is called.
        (note: each method below must match an input method in tutovnigomoku.action.php)
    */

    public function playStone(int $coord_x, int $coord_y): void
    {
        // Check that this is player's turn and that it is a "possible action" at this game state (see states.inc.php)
        self::checkAction('playStone');

        $player_id = self::getActivePlayerId();

        // Check that this intersection is free
        $sql = "SELECT
                    id, coord_x, coord_y, stone_color
                FROM
                    intersection 
                WHERE 
                    coord_x = $coord_x 
                    AND coord_y = $coord_y
                    AND stone_color is null
               ";
        $intersection = self::getObjectFromDb($sql);
        if ($intersection === null) {
            throw new BgaUserException(self::_("There is already a stone on this intersection, you can't play there"));
        }

        // Get player color
        $sql = "SELECT
                    player_id, player_color
                FROM
                    player 
                WHERE 
                    player_id = $player_id
               ";
        $player = self::getNonEmptyObjectFromDb($sql);
        $color = ($player['player_color'] === 'ffffff' ? 'white' : 'black');

        // Update the intersection with a stone of the appropriate color
        $intersection_id = $intersection['id'];
        $sql = "UPDATE
                    intersection
                SET
                    stone_color = '$color'
                WHERE 
                    id = $intersection_id
               ";
        self::DbQuery($sql);

        // Notify all players
        self::notifyAllPlayers(
            'stonePlayed',
            clienttranslate('${player_name} dropped a stone on ${coord_x},${coord_y}'),
            [
                'player_id' => $player_id,
                'player_name' => self::getActivePlayerName(),
                'coord_x' => $coord_x,
                'coord_y' => $coord_y,
                'color' => $color,
                'counters' => $this->getGameCounters(self::getCurrentPlayerId()),
            ]
        );

        // Check if end of game has been met
        if ($this->checkForWin($coord_x, $coord_y, $color)) {
            // Set active player score to 1 (he is the winner)
            $sql = "UPDATE player SET player_score = 1 WHERE player_id = $player_id";
            self::DbQuery($sql);

            // Notify final score
            self::notifyAllPlayers(
                'finalScore',
                clienttranslate('${player_name} wins the game!'),
                [
                    'player_name' => self::getActivePlayerName(),
                    'player_id' => $player_id,
                    'score_delta' => 1,
                ]
            );

            // Set global variable flag to pass on the information that the game has ended
            self::setGameStateValue('end_of_game', 1);

            // End of game message
            self::notifyAllPlayers(
                'message',
                clienttranslate('Thanks for playing!'),
                []
            );
        }

        // Go to next game state
        $this->gamestate->nextState('stonePlayed');
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Game state arguments
////////////

    /*
        Here, you can create methods defined as "game state arguments" (see "args" property in states.inc.php).
        These methods function is to return some additional information that is specific to the current
        game state.
    */

    /*
    
    Example for game state "MyGameState":
    
    function argMyGameState()
    {
        // Get some values from the current game situation in database...
    
        // return values:
        return array(
            'variable1' => $value1,
            'variable2' => $value2,
            ...
        );
    }    
    */

//////////////////////////////////////////////////////////////////////////////
//////////// Game state actions
////////////

    /*
        Here, you can create methods defined as "game state actions" (see "action" property in states.inc.php).
        The action method of state X is called everytime the current game state is set to X.
    */

    function stNextPlayer()
    {
        self::trace('stNextPlayer');

        // Go to next player
        $active_player = self::activeNextPlayer();
        self::giveExtraTime($active_player);

        $this->gamestate->nextState();
    }

    function stCheckEndOfGame()
    {
        self::trace('stCheckEndOfGame');

        $transition = 'notEndedYet';

        // If there is no more free intersections, the game ends
        $sql = "SELECT id, coord_x, coord_y, stone_color FROM intersection WHERE stone_color is null";
        $free = self::getCollectionFromDb($sql);

        if (\count($free) === 0) {
            $transition = 'gameEnded';
        }

        // If the 'end of game' flag has been set, end the game
        if (self::getGameStateValue('end_of_game') === 1) {
            $transition = 'gameEnded';
        }

        $this->gamestate->nextState($transition);
    }

//////////////////////////////////////////////////////////////////////////////
//////////// Zombie
////////////

    /*
        zombieTurn:
        
        This method is called each time it is the turn of a player who has quit the game (= "zombie" player).
        You can do whatever you want in order to make sure the turn of this player ends appropriately
        (ex: pass).
        
        Important: your zombie code will be called when the player leaves the game. This action is triggered
        from the main site and propagated to the gameserver from a server, not from a browser.
        As a consequence, there is no current player associated to this action. In your zombieTurn function,
        you must _never_ use getCurrentPlayerId() or getCurrentPlayerName(), otherwise it will fail with a "Not logged" error message. 
    */

    function zombieTurn($state, $active_player)
    {
        $statename = $state['name'];

        if ($state['type'] === "activeplayer") {
            switch ($statename) {
                default:
                    $this->gamestate->nextState("zombiePass");
                    break;
            }

            return;
        }

        if ($state['type'] === "multipleactiveplayer") {
            // Make sure player is in a non blocking status for role turn
            $this->gamestate->setPlayerNonMultiactive($active_player, '');

            return;
        }

        throw new feException("Zombie mode not supported at this game state: " . $statename);
    }

///////////////////////////////////////////////////////////////////////////////////:
////////// DB upgrade
//////////

    /*
        upgradeTableDb:
        
        You don't have to care about this until your game has been published on BGA.
        Once your game is on BGA, this method is called everytime the system detects a game running with your old
        Database scheme.
        In this case, if you change your Database scheme, you just have to apply the needed changes in order to
        update the game database and allow the game to continue to run with your new version.
    
    */

    function upgradeTableDb($from_version)
    {
        // $from_version is the current version of this game database, in numerical form.
        // For example, if the game was running with a release of your game named "140430-1345",
        // $from_version is equal to 1404301345

        // Example:
//        if( $from_version <= 1404301345 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "ALTER TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB( $sql );
//        }
//        if( $from_version <= 1405061421 )
//        {
//            // ! important ! Use DBPREFIX_<table_name> for all tables
//
//            $sql = "CREATE TABLE DBPREFIX_xxxxxxx ....";
//            self::applyDbUpgradeToAllDB( $sql );
//        }
//        // Please add your future database scheme changes here
//
//


    }
}
