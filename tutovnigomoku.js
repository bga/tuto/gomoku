/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * tutovnigomoku implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on http://boardgamearena.com.
 * See http://en.boardgamearena.com/#!doc/Studio for more information.
 * -----
 *
 * tutovnigomoku.js
 *
 * tutovnigomoku user interface script
 *
 * In this file, you are describing the logic of your user interface, in Javascript language.
 *
 */

define(['dojo', 'dojo/_base/declare', 'ebg/core/gamegui', 'ebg/counter'], function (dojo, declare) {
  return declare('bgagame.tutovnigomoku', ebg.core.gamegui, {
    constructor: function () {
      console.log('tutovnigomoku constructor');

      // Here, you can init the global variables of your user interface
      // Example:
      // this.myGlobalValue = 0;       // Game constants
      this.gameConstants = null;

    },

    /*
        setup:

        This method must set up the game user interface according to current game situation specified
        in parameters.

        The method is called each time the game interface is displayed to a player, ie:
        _ when the game starts
        _ when a player refreshes the game page (F5)

        "gamedatas" argument contains all datas retrieved by your "getAllDatas" PHP method.
    */

    setup: function (gamedatas) {
      console.log('Starting game setup');

      this.gameConstants = gamedatas.constants;

      // Setting up player boards
      for (var player_id in gamedatas.players) {
        var player = gamedatas.players[player_id];

        // Setting up players boards if needed
        var player_board_div = $('player_board_' + player_id);
        dojo.place(this.format_block('jstpl_player_board', player), player_board_div);
      }

      // TODO: Set up your game interface here, according to "gamedatas"
      // Setup intersections
      for (var id in gamedatas.intersections) {
        var intersection = gamedatas.intersections[id];

        dojo.place(this.format_block('jstpl_intersection', {
          x: intersection.coord_x,
          y: intersection.coord_y,
          stone_type: (intersection.stone_color == null ? 'no_stone' : 'stone_' + intersection.stone_color),
        }), $('gmk_background'));

        var x_pix = this.getXPixelCoordinates(intersection.coord_x);
        var y_pix = this.getYPixelCoordinates(intersection.coord_y);

        this.slideToObjectPos(
          $('intersection_' + intersection.coord_x + '_' + intersection.coord_y),
          $('gmk_background'),
          x_pix,
          y_pix,
          10,
        ).play();

        if (intersection.stone_color != null) {
          // This intersection is taken, it shouldn't appear as clickable anymore
          dojo.removeClass('intersection_' + intersection.coord_x + '_' + intersection.coord_y, 'clickable');
        }
      }

      this.updateCounters(gamedatas.counters);

      // Setup game notifications to handle (see "setupNotifications" method below)
      this.setupNotifications();

      // Add events on active elements (the third parameter is the method that will be called when the event defined by the second parameter happens - this method must be declared beforehand)
      this.addEventToClass('gmk_intersection', 'onclick', 'onClickIntersection');

      console.log('Ending game setup');
    },


    ///////////////////////////////////////////////////
    //// Game & client states

    // onEnteringState: this method is called each time we are entering into a new game state.
    //                  You can use this method to perform some user interface changes at this moment.
    //
    onEnteringState: function (stateName, args) {
      console.log('Entering state: ' + stateName);

      switch (stateName) {
        case 'playerTurn':
          if (this.isCurrentPlayerActive()) {
            var queueEntries = dojo.query('.no_stone');
            for (var i = 0; i < queueEntries.length; i++) {
              dojo.addClass(queueEntries[i], 'clickable');
            }
          }
      }
    },

    // onLeavingState: this method is called each time we are leaving a game state.
    //                 You can use this method to perform some user interface changes at this moment.
    //
    onLeavingState: function (stateName) {
      console.log('Leaving state: ' + stateName);

      switch (stateName) {

        /* Example:

        case 'myGameState':

            // Hide the HTML block we are displaying only during this game state
            dojo.style( 'my_html_block_id', 'display', 'none' );

            break;
       */


        case 'dummmy':
          break;
      }
    },

    // onUpdateActionButtons: in this method you can manage "action buttons" that are displayed in the
    //                        action status bar (ie: the HTML links in the status bar).
    //
    onUpdateActionButtons: function (stateName, args) {
      console.log('onUpdateActionButtons: ' + stateName);

      if (this.isCurrentPlayerActive()) {
        switch (stateName) {
          /*
                           Example:

                           case 'myGameState':

                              // Add 3 action buttons in the action status bar:

                              this.addActionButton( 'button_1_id', _('Button 1 label'), 'onMyMethodToCall1' );
                              this.addActionButton( 'button_2_id', _('Button 2 label'), 'onMyMethodToCall2' );
                              this.addActionButton( 'button_3_id', _('Button 3 label'), 'onMyMethodToCall3' );
                              break;
          */
        }
      }
    },

    ///////////////////////////////////////////////////
    //// Utility methods
    getXPixelCoordinates: function (intersection_x) {
      return this.gameConstants['X_ORIGIN'] + intersection_x * (this.gameConstants['INTERSECTION_WIDTH'] + this.gameConstants['INTERSECTION_X_SPACER']);
    },

    getYPixelCoordinates: function (intersection_y) {
      return this.gameConstants['Y_ORIGIN'] + intersection_y * (this.gameConstants['INTERSECTION_HEIGHT'] + this.gameConstants['INTERSECTION_Y_SPACER']);
    },

    ///////////////////////////////////////////////////
    //// Player's action

    /*

        Here, you are defining methods to handle player's action (ex: results of mouse click on
        game objects).

        Most of the time, these methods:
        _ check the action is possible at this game state.
        _ make a call to the game server

    */

    onClickIntersection: function (evt) {
      console.log('$$$$ Event : onClickIntersection');
      dojo.stopEvent(evt);

      if (!this.checkAction('playStone')) {
        return;
      }

      var node = evt.currentTarget.id;
      var coord_x = node.split('_')[1];
      var coord_y = node.split('_')[2];

      console.log('$$$$ Selected intersection : (' + coord_x + ', ' + coord_y + ')');

      if (this.isCurrentPlayerActive()) {
        this.ajaxcall(
          '/tutovnigomoku/tutovnigomoku/playStone.html',
          { lock: true, coord_x: coord_x, coord_y: coord_y },
          this,
          function (result) {
          },
          function (is_error) {
          },
        );
      }
    },


    ///////////////////////////////////////////////////
    //// Reaction to cometD notifications

    /*
        setupNotifications:

        In this method, you associate each of your game notifications with your local method to handle it.

        Note: game notification names correspond to "notifyAllPlayers" and "notifyPlayer" calls in
              your tutovnigomoku.game.php file.

    */
    setupNotifications: function () {
      dojo.subscribe('stonePlayed', this, 'notif_stonePlayed');

      dojo.subscribe('finalScore', this, 'notif_finalScore');
      this.notifqueue.setSynchronous('finalScore', 1500);
    },

    notif_stonePlayed: function (notif) {
      console.log('**** Notification : stonePlayed');
      console.log(notif);

      // Create a stone
      dojo.place(this.format_block('jstpl_stone', {
        stone_type: 'stone_' + notif.args.color, x: notif.args.coord_x, y: notif.args.coord_y,
      }), $('intersection_' + notif.args.coord_x + '_' + notif.args.coord_y));

      // Place it on the player panel
      this.placeOnObject(
        $('stone_' + notif.args.coord_x + '_' + notif.args.coord_y),
        $('player_board_' + notif.args.player_id),
      );

      // Animate a slide from the player panel to the intersection
      dojo.style('stone_' + notif.args.coord_x + '_' + notif.args.coord_y, 'zIndex', 1);
      var slide = this.slideToObject(
        $('stone_' + notif.args.coord_x + '_' + notif.args.coord_y),
        $('intersection_' + notif.args.coord_x + '_' + notif.args.coord_y),
        1000,
      );
      dojo.connect(slide, 'onEnd', this, dojo.hitch(this, function () {
        // At the end of the slide, update the intersection
        dojo.removeClass('intersection_' + notif.args.coord_x + '_' + notif.args.coord_y, 'no_stone');
        dojo.addClass('intersection_' + notif.args.coord_x + '_' + notif.args.coord_y, 'stone_' + notif.args.color);
        dojo.removeClass('intersection_' + notif.args.coord_x + '_' + notif.args.coord_y, 'clickable');

        // We can now destroy the stone since it is now visible through the change in style of the intersection
        dojo.destroy('stone_' + notif.args.coord_x + '_' + notif.args.coord_y);
      }));
      slide.play();

      this.updateCounters(notif.args.counters);
    },

    notif_finalScore: function (notif) {
      console.log('**** Notification : finalScore');
      console.log(notif);

      // Update score
      this.scoreCtrl[notif.args.player_id].incValue(notif.args.score_delta);
    },
  });
});
