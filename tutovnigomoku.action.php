<?php
/**
 *------
 * BGA framework: © Gregory Isabelli <gisabelli@boardgamearena.com> & Emmanuel Colin <ecolin@boardgamearena.com>
 * tutovnigomoku implementation : © <Your name here> <Your email address here>
 *
 * This code has been produced on the BGA studio platform for use on https://boardgamearena.com.
 * See http://en.doc.boardgamearena.com/Studio for more information.
 * -----
 *
 * tutovnigomoku.action.php
 *
 * tutovnigomoku main action entry point
 *
 *
 * In this file, you are describing all the methods that can be called from your
 * user interface logic (javascript).
 *
 * If you define a method "myAction" here, then you can call it from your javascript code with:
 * this.ajaxcall( "/tutovnigomoku/tutovnigomoku/myAction.html", ...)
 *
 */


class action_tutovnigomoku extends APP_GameAction
{
    // Constructor: please do not modify
    public function __default()
    {
        if (self::isArg('notifwindow')) {
            $this->view = 'common_notifwindow';
            $this->viewArgs['table'] = self::getArg('table', AT_posint, true);
        } else {
            $this->view = 'tutovnigomoku_tutovnigomoku';
            self::trace('Complete reinitialization of board game');
        }
    }

    // TODO: defines your action entry points there
    public function playStone()
    {
        self::setAjaxMode();
        $coord_x = self::getArg('coord_x', AT_posint, true);
        $coord_y = self::getArg('coord_y', AT_posint, true);
        $this->game->playStone($coord_x, $coord_y);

        self::ajaxResponse();
    }
}
  

